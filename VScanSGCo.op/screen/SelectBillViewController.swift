//
//  SelectBillViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 05/05/2021.
//

import UIKit
import Foundation
import JGProgressHUD

class SelectBillViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var bills: [Bill] = []
    var hdGroup = ""
    var idBill: Int?
    var typeBill: String?
    let hud = JGProgressHUD()
    
    @IBOutlet weak var billTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        title = "CHỌN LOẠI HOÁ ĐƠN"
        loadBill()
        billTableView.dataSource = self
        billTableView.delegate = self
        hud.textLabel.text = "Đang tải thông tin đợt"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        for i in 0..<bills.count {
            bills[i].totalPhoto = 0
            let fm = FileManager.default
            guard let directory = fm.urls(for: .documentDirectory, in: .userDomainMask).first else {
                return
            }
            do {
                let items = try fm.contentsOfDirectory(atPath: directory.path + "/sgcimage")
                if !items.isEmpty {
                    for file in items {
                        let arrayName = file.split(separator: "_")
                        let timePhoto = file.replacingOccurrences(of: "_", with: "").substring(to: 14)
                        let timeNow = CommonUtils.getServerDatetime(simpleDateFormat: "yyyyMMddHHmmss")
                        if arrayName[4] == "\(bills[i].id!)" &&
                            (Int(timeNow)! - Int(timePhoto)!) > 500 {
                            bills[i].totalPhoto = bills[i].totalPhoto + 1
                        }
                    }
                }
            }catch {
                print("lỗi đọc file")
            }
        }
        
        billTableView.reloadData()
        
        if hdGroup != "" {
            let defaults = UserDefaults.standard
            let arrayReport = hdGroup.split(separator: "|")
            if arrayReport.count == 5 {
                if arrayReport[0] == "sgcoop" &&
                    arrayReport[1] == defaults.string(forKey: DefaultsKeys.idCusNewKey)! &&
                    arrayReport[2] == "bienban" &&
                    typeBill == "Đính kèm biên bản" {
                    DispatchQueue.main.asyncAfter(deadline: .now()) { [self] in
                        navigationToCamera(iPhoto: 0,
                                                iBatch: 0,
                                                user: defaults.string(forKey: DefaultsKeys.usernameKey)!,
                                                typeBill: typeBill! + " " + arrayReport[4],
                                                idBill: String(idBill!),
                                                groupHD: String(arrayReport[3]))
                    }
                    return
                }else if arrayReport[0] == "sgcoop" &&
                            arrayReport[1] == defaults.string(forKey: DefaultsKeys.idCusNewKey)! &&
                            arrayReport[2] == "HD" &&
                            typeBill == "Đính kèm hóa đơn" {
                    DispatchQueue.main.asyncAfter(deadline: .now()) { [self] in
                        navigationToCamera(iPhoto: 0,
                                                iBatch: 0,
                                                user: defaults.string(forKey: DefaultsKeys.usernameKey)!,
                                                typeBill: typeBill! + " " + arrayReport[4],
                                                idBill: String(idBill!),
                                                groupHD: String(arrayReport[3]))
                    }
                    return
                }
            }
            Toast.show(message: "Mã QR không đúng", controller: self)
        }
    }

    private func loadBill(){
        MGConnection.request(APIRouter.bills, BillResponse.self, completion: {(result, err) in
            guard err == nil else {
                Toast.show(message: (err?.mErrorMessage)!, controller: self)
                return
            }
            if result?.message! == "success" {
                self.bills = (result?.bills!)!
                DispatchQueue.main.async {
                    self.billTableView.reloadData()
                }
            }else {
                Toast.show(message: "Error load bills", controller: self)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bills.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if bills[indexPath.row].id == 9 || bills[indexPath.row].id == 12 {
            cell?.textLabel?.text = bills[indexPath.row].nameBill
        }else {
            cell?.textLabel?.text = "\(bills[indexPath.row].nameBill!) (\(bills[indexPath.row].totalPhoto))"
            if bills[indexPath.row].totalPhoto > 0 {
                cell?.backgroundColor = UIColor.orange
                cell?.textLabel?.textColor = UIColor.white
            }else {
                cell?.backgroundColor = UIColor.white
                cell?.textLabel?.textColor = UIColor.black
            }
            
        }
        return cell!
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        CommonUtils.timeOut = 3600
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        CommonUtils.timeOut = 3600
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        CommonUtils.timeOut = 3600
        let defaults = UserDefaults.standard
        idBill = bills[indexPath.row].id
        typeBill = bills[indexPath.row].nameBill
        if bills[indexPath.row].id == 9 || bills[indexPath.row].id == 12 {
            navigationToQRScanner()
        }else {
            hud.show(in: self.view)
            MGConnection.request(APIRouter.batch(date: CommonUtils.getServerDatetime(simpleDateFormat: "yyyyMMdd"), typeInvoice: String(bills[indexPath.row].id!)), BatchResponse.self, completion: {(result, err) in
                guard err == nil else {
                    DispatchQueue.main.async {
                        self.hud.dismiss(afterDelay: 0)
                    }
                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                    return
                }
                DispatchQueue.main.async {
                    var countPhoto = 0
                    if result?.message == "yes" {
                        //Cập nhập app
                    }else {
                        let fm = FileManager.default
                        guard let directory = fm.urls(for: .documentDirectory, in: .userDomainMask).first else {
                            return
                        }
                        do {
                            let items = try fm.contentsOfDirectory(atPath: directory.path + "/sgcimage")
                            if !items.isEmpty {
                                for file in items {
                                    let arrayName = file.split(separator: "_")
                                    if arrayName[4] == String(self.bills[indexPath.row].id!) &&
                                        arrayName[0] == CommonUtils.getServerDatetime(simpleDateFormat: "yyyyMMdd") &&
                                        arrayName[7] == String(format: "%02d", result!.batch!){
                                        countPhoto = countPhoto + 1
                                    }
                                }
                            }
                        } catch {
                            print("lỗi đọc file")
                        }
                        print("countPhotoSelectBill: \(countPhoto)")
                        DispatchQueue.main.async {
                            self.hud.dismiss(afterDelay: 0)
                            self.navigationToCamera(iPhoto: result!.numberPhoto! + countPhoto,
                                                    iBatch: result!.batch!,
                                                    user: defaults.string(forKey: DefaultsKeys.usernameKey)!,
                                                    typeBill: self.bills[indexPath.row].nameBill!,
                                                    idBill: String(self.bills[indexPath.row].id!),
                                                    groupHD: "")
                        }
                    }
                }
            })
        }
    }
    
    private func navigationToCamera(iPhoto: Int, iBatch: Int, user: String, typeBill: String, idBill: String, groupHD: String) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let cameraScreen = sb.instantiateViewController(identifier: "cameraview") as! CameraViewController
        cameraScreen.iPhoto = iPhoto
        cameraScreen.iBatch = iBatch
        cameraScreen.user = user
        cameraScreen.typeBill = typeBill
        cameraScreen.idBill = idBill
        cameraScreen.groupHD = groupHD
        self.navigationController?.pushViewController(cameraScreen, animated: false)
    }
    
    private func navigationToQRScanner() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let qrScreen = sb.instantiateViewController(identifier: "qrscanner") as! QRScannerController
        self.navigationController?.pushViewController(qrScreen, animated: true)
    }
}
