//
//  SlideLeafViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 09/06/2021.
//

import UIKit

fileprivate enum SlideLeafConst {
    static let minimumLineSpacing: CGFloat = 20
    static let cellBothEndSpacing: CGFloat = minimumLineSpacing / 2
}

@objc public protocol SlideLeafViewControllerDelegate: class {
    func tapImageDetailView(slideLeaf: SlidePhoto, pageIndex: Int)
    @objc optional func longPressImageView(slideLeafViewController: SlideLeafViewController, slideLeaf: SlideLeaf, pageIndex: Int)
    @objc optional func slideLeafViewControllerDismissed(slideLeaf: SlideLeaf, pageIndex: Int)
}

open class SlideLeafViewController: UIViewController {
    
    open override var prefersStatusBarHidden: Bool {
        return true
    }

    open func prefersHomeIndicatorAutoHidden() -> Bool {
        return isPrefersHomeIndicatorAutoHidden
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    open override var shouldAutorotate: Bool {
        return isShouldAutorotate
    }
    
    
}
