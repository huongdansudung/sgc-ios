//
//  UploadImageViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 11/06/2021.
//

import UIKit
import JGProgressHUD

class UploadImageViewController: UIViewController {
    
    var photoLocals: [PhotoMutil] = []
    var idBill: String?
    var batch: Int?
    var typeBill: String?
    let hud = JGProgressHUD()
    
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hud.textLabel.text = "Đang upload danh sách ảnh"
    
        title = typeBill!
        let date = Date()
        let dateFormatterLocal = DateFormatter()
        dateFormatterLocal.dateFormat = "yyyyMMdd"
        photoLocals = getLocalViewStatus(date: dateFormatterLocal.string(from: date), idBill: idBill!, batch: 0)
        myTableView.dataSource = self
        myTableView.delegate = self
    }
    
    @IBAction func buttonUpload(_ sender: Any) {
        let date = Date()
        let dateFormatterLocal = DateFormatter()
        dateFormatterLocal.dateFormat = "yyyyMMdd"
        photoLocals = getLocalViewStatus(date: dateFormatterLocal.string(from: date), idBill: idBill!, batch: 0)
        if !photoLocals.isEmpty {
            let filenameFirt = photoLocals.first?.imageName
            let src = CommonUtils.src?.appendingPathComponent("\(filenameFirt!)").path
            self.hud.show(in: self.view)
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: src!) {
                uploadFilename(image: UIImage(contentsOfFile: src!)!, filename: filenameFirt!)
            }else {
                myTableView.reloadData()
                let filename2 = photoLocals.first?.imageName
                let src2 = CommonUtils.src?.appendingPathComponent("\(filename2!)").path
                uploadFilename(image: UIImage(contentsOfFile: src2!)!, filename: filenameFirt!)
            }
        }else {
            Toast.show(message: "Không có ảnh để upload", controller: self)
        }
    }
    
    private func getLocalViewStatus(date: String, idBill: String, batch: Int) -> [PhotoMutil] {
        var photoMutilLocal: [PhotoMutil] = []
        var countphoto = 0
        var countPhotoByBatch = 0
        let fm = FileManager.default
        guard let directory = fm.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return photoLocals
        }
        do {
            let items = try fm.contentsOfDirectory(atPath: directory.path + "/sgcimage")
            if !items.isEmpty {
                //đếm tổng ảnh
                for file in items {
                    let arrayName = file.split(separator: "_")
                    if arrayName[4] == idBill &&
                        arrayName[0] == date{
                        countphoto = countphoto + 1
                    }
                }
                if countphoto > 0 {
                    
                    for file in items {
                        let arrayName = file.split(separator: "_")
                        let timePhoto = file.replacingOccurrences(of: "_", with: "").substring(to: 14)
                        let timeNow = CommonUtils.getServerDatetime(simpleDateFormat: "yyyyMMddHHmmss")
                        if batch != 0 {
                            if arrayName[7] == String(format: "%02d", batch) &&
                                arrayName[4] == idBill &&
                                arrayName[0] == date &&
                                (Int(timeNow)! - Int(timePhoto)!) > 500 {
                                photoMutilLocal.append(PhotoMutil(imageName: file, url: (CommonUtils.src?.appendingPathComponent(file).path)!))
                                countPhotoByBatch = countPhotoByBatch + 1
                            }
                        }else {
                            if arrayName[4] == idBill &&
                                arrayName[0] == date &&
                                (Int(timeNow)! - Int(timePhoto)!) > 500 {
                                photoMutilLocal.append(PhotoMutil(imageName: file, url: (CommonUtils.src?.appendingPathComponent(file).path)!))
                                countPhotoByBatch = countPhotoByBatch + 1
                            }
                        }
                    }
                }else {
                    Toast.show(message: "Không có ảnh nào của loại hoá đơn và đợt này", controller: self)
                }
            }else {
                Toast.show(message: "Không có ảnh nào trong máy", controller: self)
            }
        } catch {
            Toast.show(message: "lỗi đọc file", controller: self)
        }
        return photoMutilLocal
    }
    
    func uploadFilename(image: UIImage, filename: String) {
        MGConnection.request(APIRouter.uploadFilename(imageFilename: filename), UploadFilenameResponse.self, completion: {(result, err) in
            guard err == nil else {
                DispatchQueue.main.sync {
                    self.hud.dismiss(afterDelay: 0)
                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                }
                return
            }
            if result?.message != nil {
                do {
                    try self.uploadFilePhoto(image: image, filename: result!.message!)
                    self.photoLocals.remove(at: 0)
                    self.myTableView.reloadData()
                }catch {
                    print(error.localizedDescription)
                }
            }
        })
    }
    
    func uploadFilePhoto(image: UIImage, filename: String) throws {
        MGConnection.upload(image: image, filename: filename, groupHD: "", APIRouter.uploadFilePhoto, UploadFilenameResponse.self, completion: {(result, err) in
            guard err == nil else {
                self.hud.dismiss(afterDelay: 0)
                Toast.show(message: "Upload ảnh thất bại vui lòng kiểm tra lại internet và báo cho bộ phận IT.", controller: self)
                return
            }
            if result?.message != nil {
                if result?.message! == "Error" {
                    self.hud.dismiss(afterDelay: 0)
                    Toast.show(message: "Upload ảnh thất bại vui lòng kiểm tra lại internet và báo cho bộ phận IT.", controller: self)
                }else {
                    if !self.photoLocals.isEmpty {
                        let filenameFirt = self.photoLocals.first?.imageName
                        let src = CommonUtils.src?.appendingPathComponent("\(filenameFirt!)").path
                        self.hud.show(in: self.view)
                        self.uploadFilename(image: UIImage(contentsOfFile: src!)!, filename: filenameFirt!)
                    }else {
                        self.myTableView.reloadData()
                        self.hud.dismiss()
                        Toast.show(message: "Đã upload hết ảnh.", controller: self)
                    }
                    self.removeFile(filePath: (CommonUtils.src?.appendingPathComponent((result?.message)!).path)!)
                }
            }
        })
    }
    
    func removeFile(filePath: String) {
        do {
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                try fileManager.removeItem(atPath: filePath)
                print("xoá file thành công")
            }else {
                print("File does not exist")
            }
        }catch {
            print("Could not file: \(error)")
        }
    }
}

extension UploadImageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photoLocals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
        cell?.textLabel?.sizeToFit()
        cell?.textLabel?.text = photoLocals[indexPath.row].imageName
        return cell!
    }
    
}
