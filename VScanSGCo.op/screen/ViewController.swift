//
//  ViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 03/05/2021.
//

import UIKit
import Foundation
import GTAlertCollection

class ViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        GTAlertCollection.shared.hostViewController = self
        loadLoginData()
        navigationController?.setToolbarHidden(false, animated: false)
        CommonUtils.navigationController = self.navigationController
    }
    
    @IBAction func messageVersion(_ sender: Any) {
        GTAlertCollection.shared.presentSingleButtonAlert(withTitle: "Nội dung cập nhật", message: ".....", buttonTitle: "OK",actionHandler: {})
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }

    @IBAction func btnLogin(_ sender: Any) {
        DispatchQueue.main.asyncDeduped(target: self, after: 0.5, execute: { [self] in
            let username = txtUsername.text
            let password = txtPassword.text
            let version = "1.0.0"
            let deviceMAC = UIDevice.current.identifierForVendor?.uuidString
            if (username == "" || username == nil) {
                Toast.show(message: "Vui lòng nhập username", controller: self)
            }else if (password == "" || password == nil) {
                Toast.show(message: "Vui lòng nhập password", controller: self)
            }else {
                MGConnection.request(APIRouter.login(username: username!, password: password!, version: version, deviceMac: deviceMAC!), Auth.self, completion: {(result, err) in
                    guard err == nil else {
                        Toast.show(message: (err?.mErrorMessage)!, controller: self)
                        return
                    }
                    switch result?.message! {
                    case "version new":
                        print("version new")
                    case "Access Denied":
                        DispatchQueue.main.async {
                            Toast.show(message: "User này đã đăng nhập ở thiết bị khác.", controller: self)
                        }
                    case "Auth successful":
                        if result?.role == "1" {
                            DispatchQueue.main.async {
                                self.saveLoginData(
                                    token: (result?.accessToken)!,
                                    refeshToken: (result?.refreshToken)!,
                                    username: username!,
                                    password: password!,
                                    idUser: (result?.id)!,
                                    idCus: (result?.idCus)!,
                                    idCusNew: (result?.idCus_new)!
                                )
                                self.navigationToSelectOption(nameCoop: (result?.name)!, centerCoop: (result?.center)!)
                            }
                        }else {
                            Toast.show(message: "Tài khoản này không có quyền đăng nhập.", controller: self)
                        }
                    default:
                        DispatchQueue.main.async {
                            Toast.show(message: "Tài khoản hoặc mật khẩu không chính xác.", controller: self)
                        }
                    }
                })
            }
        })
    }
    
    private func saveLoginData(token: String, refeshToken: String, username: String, password: String, idUser: Int, idCus: Int, idCusNew: Int){
        let defaults = UserDefaults.standard
        defaults.setValue(token, forKey: DefaultsKeys.tokenKey)
        defaults.setValue(refeshToken, forKey: DefaultsKeys.refreshTokenKey)
        defaults.setValue(username, forKey: DefaultsKeys.usernameKey)
        defaults.setValue(password, forKey: DefaultsKeys.passwordKey)
        defaults.setValue(idUser, forKey: DefaultsKeys.idUserKey)
        defaults.setValue(idCus, forKey: DefaultsKeys.idCusKey)
        defaults.setValue(idCusNew, forKey: DefaultsKeys.idCusNewKey)
    }
    
    private func loadLoginData() {
        let defaults = UserDefaults.standard
        if let username = defaults.string(forKey: DefaultsKeys.usernameKey) {
            txtUsername.text = username
        }
        if let password = defaults.string(forKey: DefaultsKeys.passwordKey) {
            txtPassword.text = password
        }
    }
    
    private func navigationToSelectOption(nameCoop: String, centerCoop: String){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let selectOptionScreen = sb.instantiateViewController(identifier: "SelectOption")
            as! SelectOptionViewController
        selectOptionScreen.nameCoop = nameCoop
        selectOptionScreen.centerCoop = centerCoop
        self.navigationController?.pushViewController(selectOptionScreen, animated: true)
    }
}

extension DispatchQueue {
    public func asyncDeduped(target: AnyObject, after delay: TimeInterval, execute work: @escaping @convention(block) () -> Void) {
        let dedupeIdentifier = DispatchQueue.dedupeIdentifierFor(target)
        if let existingWorkItem = DispatchQueue.workItems.removeValue(forKey: dedupeIdentifier) {
            existingWorkItem.cancel()
            NSLog("Deduped work item: \(dedupeIdentifier)")
        }
        let workItem = DispatchWorkItem {
            DispatchQueue.workItems.removeValue(forKey: dedupeIdentifier)

            for ptr in DispatchQueue.weakTargets.allObjects {
                if dedupeIdentifier == DispatchQueue.dedupeIdentifierFor(ptr as AnyObject) {
                    work()
                    NSLog("Ran work item: \(dedupeIdentifier)")
                    break
                }
            }
        }

        DispatchQueue.workItems[dedupeIdentifier] = workItem
        DispatchQueue.weakTargets.addPointer(Unmanaged.passUnretained(target).toOpaque())

        asyncAfter(deadline: .now() + delay, execute: workItem)
    }
}

private extension DispatchQueue {

    static var workItems = [AnyHashable : DispatchWorkItem]()

    static var weakTargets = NSPointerArray.weakObjects()

    static func dedupeIdentifierFor(_ object: AnyObject) -> String {
        return "\(Unmanaged.passUnretained(object).toOpaque())." + String(describing: object)
    }

}
