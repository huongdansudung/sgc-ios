//
//  MutilPhotoViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 20/05/2021.
//

import UIKit
import DropDown
import Serrata
import Kingfisher

class MutilPhotoViewController: UIViewController {
    
    let menu: DropDown = {
        let menu = DropDown()
        menu.dataSource = ["Not Upload", "Uploaded"]
        let image = ["ic_action_not_upload", "ic_action_uploaded"]
        menu.cellNib = UINib(nibName: "DropDownCell", bundle: nil)
        menu.customCellConfiguration = { index, title, cell in
            guard let cell = cell as? MyCell else {
                return
            }
            cell.myImageView.image = UIImage(named: image[index])
        }
        return menu
    }()
    
    let datePicker = UIDatePicker()
    var typeBill: String?
    var idBill: String?
    var user: String?
    var batchs: [String] = []
    let batchPicker = UIPickerView()
    var photoMutils: [PhotoMutil] = []
    var date: String?
    var batch: Int = 0
    var datePick: String?
    var isNotUpload = true
    var totalPage = 0
    var currentPage = 1
    var nextpage = 0
    
    @IBOutlet weak var mutilPhotoTableview: UITableView!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lbltTypeBill: UILabel!
    @IBOutlet weak var txfDate: UITextField!
    @IBOutlet weak var lblTotalPhoto: UILabel!
    @IBOutlet weak var txfBatch: UITextField!
    @IBOutlet weak var btnSelectUpload: UIButton!
    @IBOutlet weak var viewOne: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationStyle = .fullScreen
        //Gắn menu upload
        menu.anchorView = viewOne
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapTopItem))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        btnSelectUpload.addGestureRecognizer(gesture)
        menu.selectionAction = { index, title in
            CommonUtils.timeOut = 3600
            if index == 0 && self.isNotUpload != true {
                self.isNotUpload = true
                self.btnSelectUpload.setImage(#imageLiteral(resourceName: "ic_action_not_upload.png"), for: .normal)
                self.batchs.insert("ALL photo", at: 0)
                self.batchPicker.reloadAllComponents()
                self.batchPicker.selectRow(self.batch, inComponent: 0, animated: true)
                if self.isNotUpload == true {
                    self.photoMutils.removeAll()
                    self.photoMutils = self.getLocalViewStatus(date: self.datePick!, idBill: self.idBill!, batch: self.batch)
                    self.mutilPhotoTableview.reloadData()
                }else {
                    self.photoMutils.removeAll()
                    self.getViewStatus(typeBill: self.idBill!, date: self.datePick!, batch: self.batch, page: 1)
                }
            }
            
            if index == 1 && self.isNotUpload != false {
                self.btnSelectUpload.setImage(#imageLiteral(resourceName: "ic_action_uploaded.png"), for: .normal)
                self.isNotUpload = false
                self.batchs.remove(at: 0)
                if self.batch == 0 {
                    self.batch = self.batch + 1
                }
                self.batchPicker.reloadAllComponents()
                self.batchPicker.selectRow(self.batch - 1, inComponent: 0, animated: true)
                self.txfBatch.text = "Đợt \(self.batch)"
                if self.isNotUpload == true {
                    self.photoMutils.removeAll()
                    self.photoMutils = self.getLocalViewStatus(date: self.datePick!, idBill: self.idBill!, batch: self.batch)
                    self.mutilPhotoTableview.reloadData()
                }else {
                    self.photoMutils.removeAll()
                    self.getViewStatus(typeBill: self.idBill!, date: self.datePick!, batch: self.batch, page: 1)
                }
            }
        }
        
        mutilPhotoTableview.rowHeight = 44
        mutilPhotoTableview.dataSource = self
        mutilPhotoTableview.delegate = self
        
        lblUser.text = user
        lbltTypeBill.text = typeBill
        createDatePicker()
        createPickViewBatch()
        showDefaultValuePickView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Load dữ liệu local lần đầu
        photoMutils = getLocalViewStatus(date: datePick!, idBill: idBill!, batch: batch)
        self.mutilPhotoTableview.reloadData()
    }
    
    @IBAction func buttonReUpload(_ sender: Any) {
        if isNotUpload {
            if photoMutils.count > 0 {
                navigationToUploadImage()
            }else {
                Toast.show(message: "Không có ảnh để upload", controller: self)
            }
        }else {
            Toast.show(message: "Hãy chọn Not Upload", controller: self)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        CommonUtils.timeOut = 3600
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        CommonUtils.timeOut = 3600
    }
    
    func createPickViewBatch() {
        let toolbar = UIToolbar()
        batchPicker.dataSource = self
        batchPicker.delegate = self
        txfBatch.inputView = batchPicker
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBatchPressed))
        toolbar.items = [btnDone]
        txfBatch.inputAccessoryView = toolbar
    }
    
    func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        txfDate.textAlignment = .center
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.locale = NSLocale.init(localeIdentifier: "vi-VN") as Locale
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([btnDone], animated: true)
        txfDate.inputAccessoryView = toolbar
        txfDate.inputView = datePicker
    }
    
    @objc func didTapTopItem() {
        CommonUtils.timeOut = 3600
        menu.show()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        menu.hide()
    }
    
    // pick ngày
    @objc func donePressed() {
        CommonUtils.timeOut = 3600
        let formatter = DateFormatter()
        formatter.locale = NSLocale.init(localeIdentifier: "vi-VN") as Locale
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        txfDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
       
        //Gắn ngày
        let date = dateFormatter(date: datePicker.date)
        self.datePick = date
        getBatch(date: date)
        
    }
    
    // pick đợt
    @objc func doneBatchPressed() {
        CommonUtils.timeOut = 3600
        let batch = batchs[batchPicker.selectedRow(inComponent: 0)]
       
        txfBatch.text = batch
        if self.isNotUpload != true {
            self.batch = batchPicker.selectedRow(inComponent: 0) + 1
        }else {
            self.batch = batchPicker.selectedRow(inComponent: 0)
        }
        
        self.view.endEditing(true)
        if self.isNotUpload == true {
            self.photoMutils.removeAll()
            self.photoMutils = getLocalViewStatus(date: self.datePick!, idBill: self.idBill!, batch: self.batch)
            self.mutilPhotoTableview.reloadData()
        }else {
            self.photoMutils.removeAll()
            getViewStatus(typeBill: idBill!, date: self.datePick!, batch: self.batch, page: 1)
        }
    }
    
    private func getBatch(date: String) {
        MGConnection.request(APIRouter.batch(date: date, typeInvoice: idBill!), BatchResponse.self, completion: {(result, err) in
            guard err == nil else {
                DispatchQueue.main.sync {
                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                }
                return
            }
            self.batchs.removeAll()
            if self.isNotUpload {
                self.batchs.append("All photo")
            }
            for n in 1...(result?.batch)! {
                self.batchs.append("Đợt \(n)")
            }
            DispatchQueue.main.async {
                self.batchPicker.reloadAllComponents();
                self.batchPicker.selectRow(0, inComponent: 0, animated: true)
                let batch = self.batchs[self.batchPicker.selectedRow(inComponent: 0)]
                self.txfBatch.text = "\(batch)"
                if self.isNotUpload {
                    self.batch = 0
                }else {
                    self.batch = 1
                }
                if self.isNotUpload == true {
                    self.photoMutils.removeAll()
                    self.photoMutils = self.getLocalViewStatus(date: self.datePick!, idBill: self.idBill!, batch: self.batch)
                    self.mutilPhotoTableview.reloadData()
                }else {
                    self.photoMutils.removeAll()
                    self.getViewStatus(typeBill: self.idBill!, date: self.datePick!, batch: self.batch, page: 1)
                }
            }
        })
    }
    
    private func showDefaultValuePickView() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = NSLocale.init(localeIdentifier: "vi-VN") as Locale
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        txfDate.text = formatter.string(from: date)
        
        //Gắn ngày
        self.datePick = dateFormatter(date: date)
        getBatch(date: self.datePick!)
    }
    
    private func dateFormatter(date: Date) -> String {
        let dateFormatterLocal = DateFormatter()
        dateFormatterLocal.dateFormat = "yyyyMMdd"
        return dateFormatterLocal.string(from: date)
    }
    
    private func getViewStatus(typeBill: String, date: String, batch: Int, page: Int) {
        MGConnection.request(APIRouter.viewStatus(typeBill: typeBill, date: date, batch: batch, page: page), PhotoMutilResponse.self, completion: {(result, err) in
            guard err == nil else {
                DispatchQueue.main.sync {
                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                }
                return
            }
    
            self.totalPage = (result?.totalPhoto)!/20
            if (result?.totalPhoto)!%20 > 0 {
                self.totalPage = self.totalPage + 1
            }
            self.lblTotalPhoto.text = "\((result?.totalPhoto)!) Ảnh"
            for item in (result?.photoMutils)! {
                self.photoMutils.append(item)
            }
            self.mutilPhotoTableview.reloadData()
        })
    }
    
    private func getLocalViewStatus(date: String, idBill: String, batch: Int) -> [PhotoMutil] {
        var photoMutilLocal: [PhotoMutil] = []
        var countphoto = 0
        var countPhotoByBatch = 0
        let fm = FileManager.default
        guard let directory = fm.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return photoMutils
        }
        do {
            let items = try fm.contentsOfDirectory(atPath: directory.path + "/sgcimage")
            if !items.isEmpty {
                //đếm tổng ảnh
                for file in items {
                    let arrayName = file.split(separator: "_")
                    if arrayName[4] == idBill &&
                        arrayName[0] == date{
                        countphoto = countphoto + 1
                    }
                }
                if countphoto > 0 {
                    
                    for file in items {
                        let arrayName = file.split(separator: "_")
                        let timePhoto = file.replacingOccurrences(of: "_", with: "").substring(to: 14)
                        let timeNow = CommonUtils.getServerDatetime(simpleDateFormat: "yyyyMMddHHmmss")
                        if batch != 0 {
                            if arrayName[7] == String(format: "%02d", batch) &&
                                arrayName[4] == idBill &&
                                arrayName[0] == date &&
                                (Int(timeNow)! - Int(timePhoto)!) > 500 {
                                photoMutilLocal.append(PhotoMutil(imageName: file, url: (CommonUtils.src?.appendingPathComponent(file).path)!))
                                countPhotoByBatch = countPhotoByBatch + 1
                            }
                        }else {
                            if arrayName[4] == idBill &&
                                arrayName[0] == date &&
                                (Int(timeNow)! - Int(timePhoto)!) > 500 {
                                photoMutilLocal.append(PhotoMutil(imageName: file, url: (CommonUtils.src?.appendingPathComponent(file).path)!))
                                countPhotoByBatch = countPhotoByBatch + 1
                            }
                        }
                    }
                }else {
                    Toast.show(message: "Không có ảnh nào của loại hoá đơn và đợt này", controller: self)
                }
            }else {
                Toast.show(message: "Không có ảnh nào trong máy", controller: self)
            }
        } catch {
            Toast.show(message: "lỗi đọc file", controller: self)
        }
        lblTotalPhoto.text = "\(countPhotoByBatch) Ảnh"
        return photoMutilLocal
    }
    
    private func navigationToUploadImage(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let uploadImageScreen = sb.instantiateViewController(identifier: "UploadImage")
            as! UploadImageViewController
        uploadImageScreen.batch = batch
        uploadImageScreen.idBill = idBill
        uploadImageScreen.typeBill = typeBill
        self.navigationController?.pushViewController(uploadImageScreen, animated: true)
    }
}

extension MutilPhotoViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return batchs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return batchs[row]
    }
}

extension MutilPhotoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photoMutils.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
        cell?.textLabel?.sizeToFit()
        cell?.textLabel?.text = photoMutils[indexPath.row].imageName
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        nextpage = photoMutils.count - 1
        if indexPath.row == nextpage && currentPage < self.totalPage {
            currentPage = currentPage + 1
            nextpage = photoMutils.count - 1
            if self.isNotUpload != true {
                self.getViewStatus(typeBill: self.idBill!, date: self.datePick!, batch: self.batch, page: currentPage)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if photoMutils[indexPath.row].imageName != "" {
            if isNotUpload {
                let slideLeafs: [SlideLeaf] = photoMutils.enumerated().map { item in
                    SlideLeaf(image: UIImage(contentsOfFile: item.element.url!),
                              title: item.element.imageName!,
                              caption: "Số thứ tự " + (item.element.imageName?.split(separator: "_")[9].split(separator: ".")[0])!)
                    
                }
                let slideImageViewController = SlideLeafViewController.make(leafs: slideLeafs,
                                                                            startIndex: indexPath.row,
                                                                            fromImageView: nil)
                slideImageViewController.delegate = self
                slideImageViewController.modalPresentationStyle = .fullScreen
                present(slideImageViewController, animated: true, completion: nil)
            }else {
                let slideLeafs: [SlideLeaf] = photoMutils.enumerated().map { item in
                    SlideLeaf(imageUrlString: item.element.url,
                              title: item.element.imageName!,
                              caption: "Số thứ tự " + (item.element.imageName?.split(separator: "_")[9].split(separator: ".")[0])!)
                    
                }
                let slideImageViewController = SlideLeafViewController.make(leafs: slideLeafs,
                                                                            startIndex: indexPath.row,
                                                                            fromImageView: nil)
                slideImageViewController.delegate = self
                slideImageViewController.modalPresentationStyle = .fullScreen
                present(slideImageViewController, animated: true, completion: nil)
            }
        }
    }
}

extension MutilPhotoViewController: SlideLeafViewControllerDelegate {
    func tapImageDetailView(slideLeaf: SlideLeaf, pageIndex: Int) {
        print(pageIndex)
        print(slideLeaf)

        let viewController = DetailViewController.make(detailTitle: slideLeaf.title)
        navigationController?.show(viewController, sender: nil)
    }

    func longPressImageView(slideLeafViewController: SlideLeafViewController, slideLeaf: SlideLeaf, pageIndex: Int) {
        print(slideLeafViewController)
        print(slideLeaf)
        print(pageIndex)
    }

    func slideLeafViewControllerDismissed(slideLeaf: SlideLeaf, pageIndex: Int) {
        print(slideLeaf)
        print(pageIndex)

        let indexPath = IndexPath(row: pageIndex, section: 0)
        mutilPhotoTableview.scrollToRow(at: indexPath, at: .middle, animated: true)
    }
}
