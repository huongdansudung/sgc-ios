//
//  Bill.swift
//  VScanSGCo.op
//
//  Created by Mac on 05/05/2021.
//

import Foundation
import ObjectMapper

class Bill: Mappable {
    var id: Int?
    var nameBill: String?
    var totalPhoto: Int = 0
    var totalPhotoString: String = "0"
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nameBill <- map["nameBill"]
        totalPhoto <- map["totalphoto"]
        totalPhotoString <- map["totalphoto"]
    }
}
