//
//  SlidePhoto.swift
//  VScanSGCo.op
//
//  Created by Mac on 09/06/2021.
//

import UIKit
import Kingfisher

protocol PhotoTestProtocol: AnyObject {
    var image: UIImage? { get set }
    var imageName: String? { get set }
    var updatedImage: ((_ image: UIImage?) -> Void)? { get set }
}

public final class SlidePhoto: NSObject, PhotoTestProtocol {
    var imageName: String?
    var updatedImage: ((UIImage?) -> Void)?
    
    var image: UIImage? {
        didSet {
            self.updatedImage?(image)
        }
    }
    
    init(_ imageUrl: String, imageName: String) {
        super.init()
        
        if let url = URL(string: imageUrl) {
            let imageResouce = ImageResource(downloadURL: url)
            KingfisherManager.shared.retrieveImage(with: imageResouce, options: nil, progressBlock: nil, completionHandler: {[weak self](image, error, cacheType, imageURL) in
                        guard let weakSelf = self else {
                            return
                        }
                        
                        if let image = image {
                            weakSelf.image = image
                            weakSelf.imageName = imageName
                        }
                    })
        }
    }

}
