//
//  APIRouter.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    // =========== Begin define api ===========
    case login(username: String, password: String, version: String, deviceMac: String)
    case bills
    case getServerDatetime
    case batch(date: String, typeInvoice: String)
    case uploadFilename(imageFilename: String)
    case uploadFilePhoto
    case viewStatus(typeBill: String, date: String, batch: Int, page: Int)
    case viewStatusMonth(date: String)
    case uploadBatch(endBatch: Int, typeBill: String)
    case refreshToken(refreshToken: String)
    
    // =========== End define api ===========
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .login, .uploadFilename, .uploadFilePhoto, .viewStatus, .uploadBatch, .refreshToken:
            return .post
        case .bills, .getServerDatetime, .batch, .viewStatusMonth:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .login:
            return "logincoop"
        case .bills:
            return "list_HD"
        case .getServerDatetime:
            return "server-datetime"
        case .batch:
            return "batch"
        case .uploadFilename:
            return "upload-filename"
        case .uploadFilePhoto:
            return "upload-image"
        case .viewStatus:
            return "view_status"
        case .viewStatusMonth:
            return "view_status_month"
        case .uploadBatch:
            return "ktt_hddt"
        case .refreshToken:
            return "refresh"
        }
        
    }
    
    // MARK: - Headers
    private var headers: HTTPHeaders {
        var headers = ["Accept": "application/json"]
        switch self {
        case .login, .getServerDatetime:
            break
        case .bills, .batch, .uploadFilename, .uploadFilePhoto, .viewStatus, .viewStatusMonth, .uploadBatch:
            headers["Authorization"] = getAuthorizationHeader(isRefresh: false)
            break
        case .refreshToken:
            headers["Authorization"] = getAuthorizationHeader(isRefresh: true)
            break
        }
        
        return headers;
    }
        
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .login(let username, let password, let version, let deviceMac):
            return [
                "username": username,
                "password": password,
                "version": version,
                "deviceMac": deviceMac
            ]
        case .bills, .getServerDatetime:
            return[:]
        case .batch(let date, let typeInvoice):
            return [
                "date": date,
                "typeInvoice": typeInvoice
            ]
        case .uploadFilename(let imageFilename):
            return [
                "imageFilename": imageFilename
            ]
        case .uploadFilePhoto:
            return [:]
        case .viewStatus(let typeBill, let date, let batch, let page):
            return [
                "typeInvoice": typeBill,
                "date": date,
                "batch": batch,
                "page": page
            ]
        case .viewStatusMonth(let date):
            return [
                "date": date
            ]
        case .uploadBatch(let endBatch, let typebill):
            return [
                "endBatch": endBatch,
                "typeInvoice": typebill
            ]
        case .refreshToken(let refreshToken):
            return [
                "refreshToken": refreshToken
            ]
        }
    }
    
    private func getAuthorizationHeader(isRefresh: Bool) -> String? {
        let defaults = UserDefaults.standard
        if isRefresh {
            let refreshToken = defaults.string(forKey: DefaultsKeys.refreshTokenKey)
            return "Bearer \(refreshToken!)"
        }else {
            let token = defaults.string(forKey: DefaultsKeys.tokenKey)
            return "Bearer \(token!)"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Production.BASE_URL.asURL()
        // setting path
        var urlRequest: URLRequest = URLRequest(url: url.appendingPathComponent(path))
        // setting method
        urlRequest.httpMethod = method.rawValue
        // setting header
        for (key, value) in headers {
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        if let parameters = parameters {
            do {
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            } catch {
                print("Encoding fail")
            }
        }

        return urlRequest
    }
}
