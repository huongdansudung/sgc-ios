//
//  MyRequestAdapter.swift
//  VScanSGCo.op
//
//  Created by Mac on 03/06/2021.
//
import UIKit
import Alamofire
import GTAlertCollection

class MyRequestAdapter: RequestRetrier, RequestAdapter {
    private var isRefreshing = false
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?) -> Void
    static let shared = MyRequestAdapter()
    let defaults = UserDefaults.standard
    
    private let lock = NSLock()
    private var requestsToRetry: [RequestRetryCompletion] = []
    var accessToken:String? = nil
    var refreshToken:String? = nil
    
    private init(){
       let sessionManager = Alamofire.SessionManager.default
       sessionManager.adapter = self
       sessionManager.retrier = self
   }

    //http://192.168.0.200:5050/
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        if let urlString = urlRequest.url?.absoluteString,
           urlString.hasPrefix("http://210.2.93.45:5020/"),
           !urlString.hasSuffix("refresh") {
            if let token = accessToken {
                urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        return urlRequest
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        let defaults = UserDefaults.standard
   
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)

            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken in
                    guard let strongSelf = self else { return }
                    if succeeded == true {
                        strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }

                        if let accessToken = accessToken {
                            strongSelf.accessToken = accessToken
                            defaults.setValue(strongSelf.accessToken, forKey: DefaultsKeys.tokenKey)
                        }
                        strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    }else {
                        strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    }
                    strongSelf.requestsToRetry.removeAll()
                    strongSelf.isRefreshing = false
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else {
            return
        }
        
        isRefreshing = true
        Alamofire.request(APIRouter.refreshToken(refreshToken: defaults.string(forKey: DefaultsKeys.refreshTokenKey)!)).responseObject {(response: DataResponse<Auth>) in
            switch response.result {
            case .success:
                if response.response?.statusCode == 200 {
                    completion(true, response.result.value?.accessToken)
                } else {
                    if response.response?.statusCode == 422{
                        CommonUtils.timeOut = 3600
                        CommonUtils.timer.invalidate()
                        CommonUtils.navigationController?.popToRootViewController(animated: true)
                        Toast.show(message: "Hết phiên làm việc", controller: CommonUtils.navigationController!)
                    }
                    completion(false, nil)
                }
                
                break
                
            case .failure(let error):
                if error is AFError {
                    CommonUtils.navigationController?.popToRootViewController(animated: true)
                    completion(false, nil)
                }
                
                break
            }
        }
    }
}
