//
//  BaseResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import ObjectMapper

class BaseResponse<T: Mappable>: Mappable {
    var message: String?
    var data: T?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        data <- map["data"]
    }

}

class BaseResponseError {
    var mErrorType: NetworkErrorType!
    var mErrorCode: Int!
    var mErrorMessage: String!
    
    init(_ errorType: NetworkErrorType,_ errorCode: Int,_ errorMessage: String) {
        mErrorType = errorType
        mErrorCode = errorCode
        mErrorMessage = errorMessage
    }
}
