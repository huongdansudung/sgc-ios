//
//  Configs.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation

struct Production {
    static let BASE_URL: String = "http://210.2.93.45:5020/"
}

enum NetworkErrorType {
    case API_ERROR
    case HTTP_ERROR
    case NETWORK_ERROR
}
