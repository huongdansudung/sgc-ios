//
//  BillResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import ObjectMapper

class BillResponse: Mappable {
    var bills: [Bill]?
    var message: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        bills <- map["result"]
        message <- map["message"]
    }
}
