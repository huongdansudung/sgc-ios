//
//  UploadFilenameResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import ObjectMapper

class UploadFilenameResponse: Mappable {
    var message: String?
    var error: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["Error"]
    }
}
