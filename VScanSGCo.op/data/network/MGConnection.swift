//
//  MGConnection.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class MGConnection {
    static let sessionManager = Alamofire.SessionManager.default
    
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    static func request<T: Mappable>(_ apiRouter: APIRouter,_ returnType: T.Type, completion: @escaping (_ result: T?, _ error: BaseResponseError?) -> Void) {
        sessionManager.adapter = MyRequestAdapter.shared
        if !isConnectedToInternet() {
            let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.NETWORK_ERROR, 402, "Không tìm thấy server, Vui lòng kiểm tra lại internet!")
            completion(nil, err)
            return
        }
        
        sessionManager.request(apiRouter).validate().responseObject {(response: DataResponse<T>) in
            switch response.result {
            case .success:
                if response.response?.statusCode == 200 {
                    completion((response.result.value), nil)
                } else {
                    let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.HTTP_ERROR, (response.response?.statusCode)!, "Request is error!")
                    completion(nil, err)
                }
                
                break
                
            case .failure(let error):
                if error is AFError {
                    let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.HTTP_ERROR, response.response!.statusCode, "Request is error!")
                    completion(nil, err)
                }
                
                break
            }
        }
    }
    
    static func upload<T: Mappable>(image: UIImage, filename: String, groupHD: String, _ apiRouter: APIRouter, _ returnType: T.Type, completion: @escaping (_ result: T?,_ error: BaseResponseError?) -> Void) {
        sessionManager.adapter = MyRequestAdapter.shared
        if !isConnectedToInternet() {
            let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.NETWORK_ERROR, 402, "Không tìm thấy server, Vui lòng kiểm tra lại internet!")
            completion(nil, err)
            return
        }
        
        sessionManager.upload(multipartFormData: {
            multipartFormData in
                if let imageData = image.jpegData(compressionQuality: 1) {
                  multipartFormData.append(imageData, withName: "image", fileName: filename, mimeType: "image/png")
                }
                multipartFormData.append((groupHD.data(using: .utf8))!, withName: "groupHD")
        }, with: apiRouter, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<T>) in
                  completion((response.result.value), nil)
                  
              }
            case .failure(let encodingError):
              let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.HTTP_ERROR, 404, "Request is error!")
              completion(nil, err)
              print("error:\(encodingError)")
            }
        })
    }
}
