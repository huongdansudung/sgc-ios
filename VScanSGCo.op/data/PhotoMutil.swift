//
//  PhotoMutil.swift
//  VScanSGCo.op
//
//  Created by Mac on 21/05/2021.
//
import Foundation
import ObjectMapper

class PhotoMutil: Mappable {
    var imageName: String?
    var url: String?
    
    init(imageName: String, url: String) {
        self.imageName = imageName
        self.url = url
    }
   
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        imageName <- map["imagename"]
        url <- map["url"]
    }
}
