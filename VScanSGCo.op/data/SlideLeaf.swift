//
//  SlidePhoto.swift
//  VScanSGCo.op
//
//  Created by Mac on 09/06/2021.
//

import UIKit

public final class SlideLeaf: NSObject {
    
    public var image: UIImage?
    public var imageUrlString: String?
    public var title: String
    
    public init(image: UIImage?, title: String = "", caption: String = "") {
        self.image = image
        self.title = title
    }
    
    public init(imageUrlString: String?, title: String = "", caption: String = "") {
        self.imageUrlString = imageUrlString
        self.title = title
    }
}
