//
//  Photo.swift
//  VScanSGCo.op
//
//  Created by Mac on 28/05/2021.
//

import Foundation
import UIKit
import Kingfisher

protocol PhotoProtocol: AnyObject {
    var image: UIImage? { get set }
    var updatedImage: ((_ image: UIImage?) -> Void)? { get set }
}

class Photo: NSObject, PhotoProtocol {
    var image: UIImage? {
        didSet {
            self.updatedImage?(image)
        }
    }
    
    var updatedImage: ((UIImage?) -> Void)?
    
    init(_ imageUrl: String) {
        super.init()
        
        if let url = URL(string: imageUrl) {
            let imageResouce = ImageResource(downloadURL: url)
            KingfisherManager.shared.retrieveImage(with: imageResouce, options: nil, progressBlock: nil, completionHandler: {[weak self](image, error, cacheType, imageURL) in
                        guard let weakSelf = self else {
                            return
                        }
                        
                        if let image = image {
                            weakSelf.image = image
                        }
                    })
        }
    }
}
