//
//  DataCollectionView.swift
//  VScanSGCo.op
//
//  Created by Mac on 08/06/2021.
//

import Foundation
import UIKit

class DataCollectionView: UICollectionViewCell {
    lazy var scalingImageView: ScalingImageView = {
        let view = ScalingImageView(frame: self.bounds)
        view.delegate = self
        return view
    }()
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
