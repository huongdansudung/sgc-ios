//
//  MyCell.swift
//  VScanSGCo.op
//
//  Created by Mac on 26/05/2021.
//

import DropDown
import UIKit

class MyCell: DropDownCell {
    @IBOutlet var myImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        myImageView.contentMode = .scaleAspectFit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
