//
//  WaitingCustom.swift
//  VScanSGCo.op
//
//  Created by Mac on 07/06/2021.
//

import UIKit

open class WaitingCustom: NSObject {
    public static let shared = WaitingCustom()

    public var alertController: UIAlertController!
    
    public var hostViewController: UIViewController!
    
    public var activityIndicator: UIActivityIndicatorView!
    
    public var progressBar: UIProgressView!
    
    public var label: UILabel!
    
    public init(withHostViewController hostViewController: UIViewController) {
        super.init()
        self.hostViewController = hostViewController
    }
    
    override init() {
        super.init()
    }
    
    fileprivate func cleanUpAlertController() {
        if alertController != nil {
            alertController = nil
            if let _ = self.progressBar { self.progressBar = nil }
            if let _ = self.label { self.label = nil }
        }
    }
    
    public func dismissAlert() {
        if let alertController = self.alertController {
            alertController.dismiss(animated: true) {
                self.alertController = nil
                
                if let _ = self.progressBar { self.progressBar = nil }
                if let _ = self.label { self.label = nil }
            }
        }
    }
    
    public func presentActivityAlert(withTitle title: String?, message: String?, activityIndicatorColor: UIColor = UIColor.black, showLargeIndicator: Bool = false) {
        // Check if the host view controller has been set.
        if (hostViewController != nil) {
            // Operate on the main thread.
                // Make sure that the alertController is nil before initializing it.
                self.cleanUpAlertController()
                
                // Initialize the alert controller.
                // If the alert message is not nil, then add additional spaces at the end, or if it's nil add just these spaces, so it's possible to make
                // room for the activity indicator.
                // If the large activity indicator is about to be shown, then add three spaces, otherwise two.
                self.alertController = UIAlertController(title: title, message: (message ?? "") + (showLargeIndicator ? "\n\n\n" : "\n\n"), preferredStyle: .alert)
                
                // Initialize the activity indicator as large or small according to the showLargeIndicator parameter value.
                self.activityIndicator = UIActivityIndicatorView(style: showLargeIndicator ? .whiteLarge : .gray)
                
                // Set the activity indicator color.
                self.activityIndicator.color = activityIndicatorColor
                
                // Start animating.
                self.activityIndicator.startAnimating()
                
                // Add it to the alert controller's view as a subview.
                self.alertController.view.addSubview(self.activityIndicator)
                
                // Specify the following constraints to make it appear properly.
                self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
                self.activityIndicator.centerXAnchor.constraint(equalTo: self.alertController.view.centerXAnchor).isActive = true
                self.activityIndicator.bottomAnchor.constraint(equalTo: self.alertController.view.bottomAnchor, constant: -8.0).isActive = true
                self.alertController.view.layoutIfNeeded()
                hostViewController.present(self.alertController, animated: false, completion: nil)
            
        } else {
            // The host view controller is nil and the alert controller cannot be presented.
            NSLog("[GTAlertCollection]: No host view controller was specified")
            
        }
    }
}
