//
//  defaultsKeys.swift
//  VScanSGCo.op
//
//  Created by Mac on 04/05/2021.
//

import Foundation

struct DefaultsKeys {
    static let tokenKey = "token"
    static let refreshTokenKey  = "refreshToken"
    static let usernameKey = "username"
    static let passwordKey = "password"
    static let idUserKey = "id"
    static let idCusKey = "idCus"
    static let idCusNewKey = "idCus_new"
}
