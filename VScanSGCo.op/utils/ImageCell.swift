//
//  ImageCell.swift
//  VScanSGCo.op
//
//  Created by Mac on 09/06/2021.
//

import UIKit

final class ImageCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleAspectFill
        }
    }

    func configure(image: UIImage) {
        imageView.image = image
    }
}
